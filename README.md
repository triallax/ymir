# ymir

## About

`ymir` is a tool for automating bulk test package builds for Void Linux.

## How it works

Under the hood, `ymir` is a wrapper around the `xbps-src` tool, with some additional niceties:

- automatically managed multiple masterdirs and concurrent build processes
- `ymir todo` tool for managing a simple text file list of packages to build
(you're also free to edit it manually)
- `ymir report` tool for inspecting the results of last build

To avoid polluting your filesystem, `ymir` keeps things like masterdirs, logdirs etc.
under a central `${XDG_STATE_HOME}/ymir` / `${HOME}/.local/state/ymir` directory.

## Config

### Xbps distdir

Most operations depend on a local `void-packages` repo (the distdir) to work, e.g. to locate the `xbps-src` tool.

`ymir` will try to locate the distdir from the first successful step in the following list:

1. from the `-D path/to/void-packages` cmdline flag
2. via `xdistdir` from [xtools](https://github.com/leahneukirchen/xtools) if available
3. from the `XBPS_DISTDIR` environment variable

## Usage

### Manage todo-list file for the build

Add Go packages to the list:

```shell
ymir todo grab -f build_style:go
```

Or specify packages explicite:

```shell
ymir todo grab -p linuxwave -p curl -p ripgrep
```

### Run the build

Run bulk build with 3 concurrent `xbps-src` processes and corresponding masterdirs:

```shell
ymir run -p3
```

### Check the results

See the statuses of the last executed build:

```shell
ymir report
```
