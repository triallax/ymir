mod status_check;

use std::collections::{HashMap, VecDeque};
use std::num::NonZeroUsize;
use std::path::Path;
use std::process::Command;
use std::time::Duration;

use anyhow::{bail, Result as AhResult};
use duct::Handle;
use wax::Glob;

use crate::cli::RunArgs;
use crate::paths;
use crate::paths::Paths;
use crate::table::{Slot, Table};

#[derive(Debug)]
struct Build {
    package: String,
    proc_handle: Handle,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum XbpsStatus {
    Good,
    Bad,
    MissingDependency,
    MissingSetId,
    BadChecksum,
    BadFetch,
}

#[derive(Debug)]
struct Params {
    procs: u64,
    jobs: u64,
    poll_period: Duration,
    masterdir_arch: Option<String>,
}

#[derive(Debug)]
pub struct Run {
    params: Params,
    paths: Paths,
    builds: Table<Build>,
    statuses: HashMap<XbpsStatus, Vec<String>>,
}

fn prepare_params(args: &RunArgs) -> Params {
    let parallelism: u64 = std::thread::available_parallelism()
        .map(NonZeroUsize::get)
        .map(|v| v as _)
        .unwrap_or(1);
    let poll_secs = args.poll_period.unwrap_or(1);

    Params {
        procs: args.procs.unwrap_or(1),
        jobs: args.jobs.unwrap_or(parallelism),
        poll_period: Duration::from_secs(poll_secs),
        masterdir_arch: args.masterdir_arch.clone(),
    }
}

pub fn clean() -> AhResult<()> {
    let glob = Glob::new("{report.toml,logs,masterdir_*}")?;
    let state_dir = paths::state_path()?;

    for entry in glob.walk(state_dir) {
        let entry = entry?;
        let path = entry.path();

        eprintln!("Removing {}", path.display());

        let metadata = entry.metadata()?;
        if metadata.is_file() {
            std::fs::remove_file(path)?;
        } else {
            std::fs::remove_dir_all(path)?;
        }
    }

    Ok(())
}

impl Build {
    pub fn determine_status(&self) -> AhResult<XbpsStatus> {
        let status = self.proc_handle.wait()?.status;

        if status.success() {
            return Ok(XbpsStatus::Good);
        }

        let log_dir = paths::logs_path()?;
        let pkg_log_dir = log_dir.join(&self.package);
        let pkg_log_file = pkg_log_dir.join("log.txt");

        let content = std::fs::read_to_string(&pkg_log_file)?;
        let checked_lines = content.lines().rev().take(2);

        for line in checked_lines {
            if status_check::is_bad_checksum(line) {
                return Ok(XbpsStatus::BadChecksum);
            }

            if status_check::is_bad_fetch(line) {
                return Ok(XbpsStatus::BadFetch);
            }

            if status_check::is_missing_dependency(line) {
                return Ok(XbpsStatus::MissingDependency);
            }

            if status_check::is_missing_set_id(line) {
                return Ok(XbpsStatus::MissingSetId);
            }
        }

        Ok(XbpsStatus::Bad)
    }
}

impl Run {
    pub fn new(args: &RunArgs, paths: Paths) -> Self {
        let params = prepare_params(args);
        let procs = params.procs;
        Self {
            params,
            paths,
            builds: Table::new(procs as _),
            statuses: HashMap::new(),
        }
    }

    pub fn statuses(&self) -> &HashMap<XbpsStatus, Vec<String>> {
        &self.statuses
    }

    fn poll_finished(&mut self) -> AhResult<Option<(Slot<'_, Build>, Build)>> {
        std::thread::sleep(self.params.poll_period);

        self.builds.try_release_slot_if(|build| {
            build
                .proc_handle
                .try_wait()
                .map(|maybe_status| maybe_status.is_some())
                .map_err(Into::into)
        })
    }

    fn bootstrap_command(&self, masterdir: &Path) -> Command {
        let xbps_src = self.paths.xbps_src();
        let mut result = Command::new(xbps_src);

        result.args(["binary-bootstrap", "-m"]);
        result.arg(masterdir);
        if let Some(arch) = &self.params.masterdir_arch {
            result.arg(arch);
        }

        result
    }

    fn finish_builds(&mut self) -> AhResult<()> {
        while !self.builds.is_empty() {
            let Some((_, build)) = self.poll_finished()? else {
                continue;
            };

            let status = build.determine_status()?;
            self.statuses
                .entry(status.clone())
                .or_default()
                .push(build.package.to_string());

            eprintln!(
                "Finished building {} with status {:?}",
                build.package, status
            );
        }

        Ok(())
    }

    pub fn boostrap_masterdirs(&self) -> AhResult<()> {
        let state_dir = paths::state_path()?;

        for n in 0..self.params.procs {
            let dir_name = format!("masterdir_{:03}", n);
            eprintln!("Bootstrapping {}", dir_name);

            let path = state_dir.join(dir_name);
            std::fs::create_dir_all(&path)?;

            let mut bootstrap_process = self.bootstrap_command(&path);
            let status = bootstrap_process.status()?;

            if !status.success() {
                bail!("Failed to bootstrap masterdir: {}", path.display());
            }
        }

        Ok(())
    }

    pub fn execute(&mut self, packages: &[String]) -> AhResult<()> {
        let state_dir = paths::state_path()?;
        let log_dir = paths::logs_path()?;
        let xbps_src = self.paths.xbps_src();
        let jobs = self.params.jobs;

        let mut last_finished = None;
        let mut packages: VecDeque<_> = packages.iter().cloned().collect();

        loop {
            let mut free_slot = match self.builds.free_slot() {
                Some(slot) => slot,
                None => {
                    let Some((slot, build)) = self.poll_finished()? else {
                        continue;
                    };

                    let status = build.determine_status()?;
                    last_finished = Some((build.package.clone(), status.clone()));

                    eprintln!(
                        "Finished building {} with status {:?}",
                        build.package, status
                    );

                    slot
                }
            };

            let Some(pkg) = packages.pop_front() else {
                if let Some((pkg, status)) = last_finished {
                    self.statuses.entry(status).or_default().push(pkg);
                }

                break;
            };

            let slot_number = free_slot.number();
            let dir_name = format!("masterdir_{:03}", slot_number);
            let masterdir = state_dir.join(&dir_name);

            eprintln!("Building {} in {}", pkg, dir_name);

            let pkg_log_dir = log_dir.join(&pkg);
            let pkg_log_file = pkg_log_dir.join("log.txt");

            std::fs::create_dir_all(&pkg_log_dir)?;

            let job_flag = format!("-j{}", jobs);
            let pkg_process = duct::cmd!(&xbps_src, "pkg", "-1", job_flag, "-m", masterdir, &pkg);
            let proc_handle = pkg_process
                .stderr_to_stdout()
                .stdout_path(&pkg_log_file)
                .unchecked()
                .start()?;

            let build = Build {
                package: pkg.to_string(),
                proc_handle,
            };

            free_slot.set(build);

            if let Some((pkg, status)) = last_finished.take() {
                self.statuses.entry(status).or_default().push(pkg);
            }
        }

        self.finish_builds()?;

        Ok(())
    }
}
