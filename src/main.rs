mod cli;
mod paths;
mod report;
mod run;
mod table;
mod todo;

use anyhow::Result as AhResult;
use clap::Parser;

use cli::{Cli, Command, TodoCommand};
use paths::Paths;
use run::Run;
use todo::Todo;

fn main() -> AhResult<()> {
    let cli = Cli::parse();
    let paths = Paths::new(&cli)?;

    let state_dir = paths::state_path()?;
    std::fs::create_dir_all(state_dir)?;

    match &cli.command {
        Command::Run(args) => {
            let packages = todo::load()?;

            if packages.is_empty() {
                eprintln!("No packages to build");
                return Ok(());
            }

            run::clean()?;

            let mut run = Run::new(args, paths);
            run.boostrap_masterdirs()?;
            run.execute(&packages)?;

            let statuses = run.statuses().clone();
            let report = report::create_report(statuses);

            report::save(&report)?;
        }
        Command::Report(args) => {
            let report = report::load()?;
            report::print(args, &report);
        }
        Command::Todo(TodoCommand::Grab(specs)) => {
            let todo = Todo::new(paths);
            let packages = todo.grab(specs)?;

            todo::save(&packages)?;
        }
        Command::Todo(TodoCommand::Clear) => todo::clean()?,
        Command::Todo(TodoCommand::List) => {
            let packages = todo::load()?;

            for package in packages {
                println!("{}", package);
            }
        }
    }

    Ok(())
}
