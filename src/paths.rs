use std::path::PathBuf;

use anyhow::{anyhow, bail, Result as AhResult};

use crate::cli::Cli;

pub const BASE_DIR: &str = "ymir";
pub const XBPS_SRC_EXECUTABLE: &str = "xbps-src";
pub const SRCPKGS_DIR: &str = "srcpkgs";

#[derive(Debug)]
pub struct Paths {
    distdir: PathBuf,
}

fn locate_distdir(cli: &Cli) -> Option<PathBuf> {
    if let Some(path) = &cli.distdir {
        return Some(path.clone());
    }

    let proc = duct::cmd!("xdestdir").stdout_capture();
    if let Ok(output) = proc.read() {
        return Some(output.into());
    }

    if let Some(path) = std::env::var_os("XBPS_DISTDIR") {
        return Some(path.into());
    }

    None
}

pub fn state_path() -> AhResult<PathBuf> {
    dirs::state_dir()
        .or_else(|| dirs::home_dir().map(|p| p.join(".local/state")))
        .map(|p| p.join(BASE_DIR))
        .ok_or_else(|| anyhow!("Failed to locate state dir for your system"))
}

pub fn logs_path() -> AhResult<PathBuf> {
    state_path().map(|p| p.join("logs"))
}

impl Paths {
    pub fn new(cli: &Cli) -> AhResult<Self> {
        let Some(distdir) = locate_distdir(cli) else {
            bail!("Failed to locate distdir");
        };

        if !distdir.exists() {
            bail!(
                "The specified distdir does not exist: {}",
                distdir.display()
            );
        }

        let result = Self {
            distdir: distdir.clone(),
        };

        Ok(result)
    }

    pub fn xbps_src(&self) -> PathBuf {
        self.distdir.join(XBPS_SRC_EXECUTABLE)
    }

    pub fn srcpkgs(&self) -> PathBuf {
        self.distdir.join(SRCPKGS_DIR)
    }
}
