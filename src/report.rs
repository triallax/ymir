use anyhow::Result as AhResult;
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::cli::ReportArgs;
use crate::paths;
use crate::run::XbpsStatus;

#[derive(Default, Serialize, Deserialize)]
pub struct Report {
    good: Vec<String>,
    bad: Vec<String>,
    bad_checksum: Vec<String>,
    bad_fetch: Vec<String>,
    missing_dependency: Vec<String>,
    missing_set_id: Vec<String>,
}

pub fn create_report(iter: impl IntoIterator<Item = (XbpsStatus, Vec<String>)>) -> Report {
    let mut report = Report::default();

    for (status, packages) in iter {
        let dest = match status {
            XbpsStatus::Good => &mut report.good,
            XbpsStatus::Bad => &mut report.bad,
            XbpsStatus::MissingDependency => &mut report.missing_dependency,
            XbpsStatus::MissingSetId => &mut report.missing_set_id,
            XbpsStatus::BadChecksum => &mut report.bad_checksum,
            XbpsStatus::BadFetch => &mut report.bad_fetch,
        };

        dest.extend(packages.into_iter().sorted());
    }

    report
}

pub fn load() -> AhResult<Report> {
    let state_dir = paths::state_path()?;
    let report_file = state_dir.join("report.toml");

    let content = std::fs::read_to_string(report_file)?;
    let result = toml::from_str(&content)?;

    Ok(result)
}

pub fn save(report: &Report) -> AhResult<()> {
    let state_dir = paths::state_path()?;
    let report_file = state_dir.join("report.toml");

    let serialized = toml::to_string(report)?;
    std::fs::write(report_file, serialized)?;

    Ok(())
}

pub fn print(args: &ReportArgs, report: &Report) {
    let good_fields = [("good", &report.good)];
    let bad_fields = [
        ("bad", &report.bad),
        ("bad_checksum", &report.bad_checksum),
        ("bad_fetch", &report.bad_fetch),
        ("missing_dependency", &report.missing_dependency),
        ("missing_set_id", &report.missing_set_id),
    ];

    let prefix = (!args.failures).then_some(good_fields);
    let selected = prefix.into_iter().flatten().chain(bad_fields);

    for (status, packages) in selected.filter(|(_, s)| !s.is_empty()) {
        println!("{}:", status);

        for pkg in packages {
            println!("\t{}", pkg);
        }
    }
}
