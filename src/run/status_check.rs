use once_cell::sync::Lazy;
use regex::Regex;

static MISSING_DEP_PATTERN: Lazy<Regex> = Lazy::new(|| {
    Regex::new("ERROR: dep .* not found: -1 passed: instructed not to build").unwrap()
});
static BAD_CHECKSUM_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new("ERROR: .*: couldn't verify distfiles, .*").unwrap());
static BAD_FETCH_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new("ERROR: .*: failed to fetch .*").unwrap());
static MISSING_SET_ID_PATTERN: Lazy<Regex> = Lazy::new(|| {
    Regex::new("=> ERROR: set[gu]id files not explicitly allowed, please list them in \\$set[gu]id")
        .unwrap()
});

pub fn is_missing_dependency(line: &str) -> bool {
    MISSING_DEP_PATTERN.is_match(line)
}

pub fn is_bad_checksum(line: &str) -> bool {
    BAD_CHECKSUM_PATTERN.is_match(line)
}

pub fn is_bad_fetch(line: &str) -> bool {
    BAD_FETCH_PATTERN.is_match(line)
}

pub fn is_missing_set_id(line: &str) -> bool {
    MISSING_SET_ID_PATTERN.is_match(line)
}
